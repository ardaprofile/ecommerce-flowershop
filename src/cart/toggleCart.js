 import { getDomElement } from '../utils.js';


const cartOverlay = getDomElement('.cart-overlay');
const closeCartBtn = getDomElement('.cart-close');

closeCartBtn.addEventListener('click',()=>{
  cartOverlay.classList.remove('show');
});

cartOverlay.addEventListener('click', (e) => {
  //console.log(cartOverlay);
  if(e.target.classList.contains('cart-overlay')){
    cartOverlay.classList.remove('show');
  }
});
export const openCart = () => {
  cartOverlay.classList.add('show');
};


