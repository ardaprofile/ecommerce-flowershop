import { getDomElement } from './utils.js';


const menuItems = getDomElement(".menuItems");
const menuIcon = getDomElement(".menu-icon");

    menuItems.style.maxHeight = "0px";

    function menutoggle()
    {
        if(menuItems.style.maxHeight == "0px")
            {
                menuItems.style.maxHeight = "300px";
            }else
            {
                menuItems.style.maxHeight = "0px"
            }
    }
    menuIcon.addEventListener('click',menutoggle);