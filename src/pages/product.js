// global imports
import '../toggleNavbar.js';
import '../cart/setupCart.js';
import fetchProducts from '../fetchProducts.js'
 import '../cart/toggleCart.js';




// specific
import { addToCart } from '../cart/setupCart.js';
import { getDomElement, formatPrice } from '../utils.js';
import { setupStore, store } from '../store.js';
import { openCart } from '../cart/toggleCart.js'; 


//selections from Dom (product.html)
const loading = getDomElement('.page-loading');

const centerDOM = getDomElement('.single-product');
const pageTitleDOM = getDomElement('.productTitle');
const imgDOM = getDomElement('.single-product-img');
const titleDOM = getDomElement('.single-product-title');
const companyDOM = getDomElement('.single-product-company');
const priceDOM = getDomElement('.single-product-price');
const descDOM = getDomElement('.single-product-desc');
const cartBtn = getDomElement('.addtoCartBtn');

let productID;

window.addEventListener('DOMContentLoaded', function(){
  const urlID = window.location.search.slice(4);
  //console.log(urlID);
  const findProduct = store.filter((product)=>product.id === urlID);
    //console.log( typeof findproduct);
    //console.log(findproduct)
    //console.log(findproduct[0].title);
if(findProduct.length === 0){
  loading.style.display = 'none';
  document.title = `Error Page | Blossom`;

  pageTitleDOM.innerHTML = `<h2>Error</h2>`;
  centerDOM.innerHTML =
  `<div>
    <h3 class="error">sorry, something went wrong</h3>
    <a href="index.html" class="btn">Back home</a>
  </div>`
}
else{

      //console.log(findProduct[0].id)
      productID = findProduct[0].id;
      document.title = `${findProduct[0].title} | Blossom`;
      pageTitleDOM.textContent = `HOME / ${findProduct[0].title}`;
      imgDOM.src = findProduct[0].image;
      titleDOM.textContent = findProduct[0].title;
      companyDOM.textContent = ` by ${findProduct[0].company}`;
      priceDOM.textContent = formatPrice(findProduct[0].price);
      descDOM.textContent = findProduct[0].desc;
      loading.style.display = 'none';

}
//console.log(findProduct.length);
});
cartBtn.addEventListener('click',function() {
  const qty = getDomElement('.quantity').value;
  const quantity = parseInt(qty);
  addToCart(productID,quantity);

 openCart();
})
