
const fetchProducts = async () =>{

  const response = await fetch("../products.json").catch(err => console.log(err));
  if(response){
    return response.json();
  }
  return response;
}

export default fetchProducts;