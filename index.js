// global imports
import './src/toggleNavbar.js';
/* import './src/cart/toggleCart.js'; */
import './src/cart/setupCart.js';


//specific imports
import { getDomElement } from './src/utils.js';
import display from './src/displayProducts.js';
import fetchProducts from './src/fetchProducts.js'
import { setupStore, store } from './src/store.js';





     VanillaTilt.init(document.querySelector(".col-left .offer-img"), {
        max: 25,
        speed: 400
      });
const init = async () =>{
    const products = await fetchProducts();
    if(products){
        setupStore(products.items);
        const newarrival = store.filter((product)=> product.newarrival===true);
        display(newarrival, getDomElement('.new-arrival'))
    }
}




window.addEventListener('DOMContentLoaded',init);



